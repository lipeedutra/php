<?php
class Carrinho{
    private $produto;
    public function Carrinho(){}
        //Adiciona um produto
    public function addProduto(Produto $m){
        $this->produto[] = $m;
    }
        // Recupera um produto pelo id
    public function getProduto(int $idProduto){
        foreach($this->produto as $pro){
            if($pro->getId() == $idProduto){
                return $pro;
            }
        }
    }
        // Remove um produto pelo id
    public function removeProduto(int $idProduto){
        for($i=0;$i < count($this->produto);$i++){
            if($this->produto[$i]->getId() == $idProduto){
                unset($this->produto[$i]);
            }
        }
    }
        // lista todos os produtos
    public function listar(){
        foreach($this->produto as $pro){
            echo "<b>Código:</b> {$pro->getId()}<br/>
                  <b>Nome:</b> {$pro->getNome()}<br/>
                  <b>Descrição:</b> {$pro->getDescricao()}<br/>
                  --------------------<br/>";
        }
    }
}
?>