<?php
include_once("config.php");
 
//Inicio o carrinho
$Carrinho = new Carrinho();
 
//Joga na sessão
$_SESSION["carrinho"] = $Carrinho;
 
//inicio dois produtos
$produto1 = new Produto(23,"Chave de Fenda","Chave de fenda blindada, com ponta em diamante");
$produto2 = new Produto(354,"Batedeira","Batedeira Braslenp, com três velocidades");
 
//Adiciona produto 1
$_SESSION["carrinho"]->addProduto($produto1);
 
//Adiciona produto 2
$_SESSION["carrinho"]->addProduto($produto2);
 
//Link para a página carrinho.php
echo "<a href=\"carrinho.php\">Meu carrinho</a>";
?>